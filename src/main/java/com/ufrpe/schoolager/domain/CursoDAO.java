package com.ufrpe.schoolager.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CursoDAO extends CrudRepository<Curso, Long> {

    @Override
    <S extends Curso> S save(S entity);

    @Override
    Optional<Curso> findById(Long aLong);

    @Override
    Iterable<Curso> findAll();

    @Override
    void deleteById(Long aLong);
}
