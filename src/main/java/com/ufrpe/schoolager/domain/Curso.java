package com.ufrpe.schoolager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "curso")
public class Curso {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "turno")
    private String turno;

    @Column(name = "numero_alunos")
    private String numeroAlunos;

    @Column(name = "numero_professores")
    private String numeroProfessores;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "professor_id", referencedColumnName = "id")
    @JsonIgnoreProperties("curso")
    private Professor professor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getNumeroAlunos() {
        return numeroAlunos;
    }

    public void setNumeroAlunos(String numeroAlunos) {
        this.numeroAlunos = numeroAlunos;
    }

    public String getNumeroProfessores() {
        return numeroProfessores;
    }

    public void setNumeroProfessores(String numeroProfessores) {
        this.numeroProfessores = numeroProfessores;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
}
