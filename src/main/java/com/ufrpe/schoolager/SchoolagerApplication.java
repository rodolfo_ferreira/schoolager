package com.ufrpe.schoolager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchoolagerApplication.class, args);
    }

}
