package com.ufrpe.schoolager.controller;

import com.ufrpe.schoolager.domain.Curso;
import com.ufrpe.schoolager.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/curso")
public class CursoController {

    private CursoService cursoService;

    @Autowired
    public CursoController(CursoService cursoService) {
        this.cursoService = cursoService;
    }

    @PostMapping(value = "/")
    public void createCurso(@RequestBody Curso curso) throws Exception {
        this.cursoService.create(curso);
    }

    @GetMapping(value = "/{id}")
    public Curso readCurso(@PathVariable Long id) throws Exception {
        return this.cursoService.read(id);
    }

    @PutMapping(value = "/{id}")
    public Curso update(@PathVariable Long id, @RequestBody Curso curso) throws Exception {
        return this.cursoService.update(id, curso);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        this.cursoService.delete(id);
    }

    @GetMapping(value = "/")
    public Iterable<Curso> readAll() {
        return this.cursoService.readAll();
    }
}
