package com.ufrpe.schoolager.service;

import com.ufrpe.schoolager.domain.Curso;
import com.ufrpe.schoolager.domain.CursoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CursoService {

    private CursoDAO cursoDAO;

    @Autowired
    public CursoService(CursoDAO cursoDAO) {
        this.cursoDAO = cursoDAO;
    }

    public Curso create(Curso curso) throws Exception {
        if (curso.getId() == null) {
            return this.cursoDAO.save(curso);
        } else {
            throw new Exception("Curso já tem um id");
        }
    }

    public Curso read(Long id) throws Exception {
        Optional<Curso> curso = this.cursoDAO.findById(id);
        if (!curso.isPresent()) {
            throw new Exception("Curso não encontrado");
        }

        return curso.get();
    }

    public Curso update(Long id, Curso curso) throws Exception {
        if (id != null) {
            curso.setId(id);
            return this.cursoDAO.save(curso);
        } else {
            throw new Exception("Curso não possui um id válido");
        }
    }

    public Iterable<Curso> readAll() {
        return this.cursoDAO.findAll();
    }

    public void delete(Long id) {
        this.cursoDAO.deleteById(id);
    }
}
